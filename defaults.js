

function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    

    for (let eachKey in defaultProps){
        if(!obj[eachKey]){
obj[eachKey] = defaultProps[eachKey]
        }
    }

    return obj
}

module.exports = defaults