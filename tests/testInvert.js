const invert = require("../invert")


const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };



console.log(invert(testObject))

let data = 
    {
      name: "Apple",
      id: 1,
      alt: [{ name: "fruit1", description: "tbd1" }]
    }

 console.log(invert(data))

console.log(`${[1,2,3]}`)


// console.log(String(["A String",1,2]));
// console.log(String(undefined));
// console.log(String(null));
// console.log(String({value: "An arbitrary object"}));
// console.log(String({toString: function(){return "An object with a toString() method";}}));
// console.log(String(function(){return "An arbitrary function"}));